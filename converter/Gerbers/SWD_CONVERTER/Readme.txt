I order Rigid Board with these parameters and files:

Name:               SWD_CONVERTER
Customer:           CTU

PCB Parameters:
Type:               FR4
Number of layers:   2 
Final thickness:    1.6 mm
Copper thickness:   35um
Finish:             HASL Lead Free
Soldermask TOP:		White
Soldermask BOT:		White
Silkscreen TOP:		Black
Silkscreen Bottom:  Black
Dimensions:			34.8 x 47.0 mm

SWD_CONVERTER.gbl            Copper - BOTTOM side 
SWD_CONVERTER.gtl            Copper - TOP side    
SWD_CONVERTER.gko            Milling of PCB
SWD_CONVERTER.gto            Silkscreen - TOP
SWD_CONVERTER.gbo            Silkscreen - BOTTOM
SWD_CONVERTER.gbs            Solder mask - BOTTOM side
SWD_CONVERTER.gts            Solder mask - TOP side
SWD_CONVERTER_NPTH.exc       Non plated drills
SWD_CONVERTER_PTH.exc        Plated drills 

Copper order:
TOP
BOTTOM 

Output testing:
Electrical testing: YES


